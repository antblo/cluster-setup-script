#!/bin/bash

printf "Website/endpoint for the cluster: "
read ENDPOINT

set -e # Exit on error.
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab # Disable swap being turned on after boot.
swapoff -a # Disable swap. Kubernetes cannot work with it on.

# Install Docker CE
apt-get update && apt-get install apt-transport-https ca-certificates curl software-properties-common

### Add Docker’s official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

### Add Docker apt repository.
add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"

## Install Docker CE.
apt-get update && apt-get install docker-ce=18.06.2~ce~3-0~ubuntu -y

# Setup daemon.
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

mkdir -p /etc/systemd/system/docker.service.d

# Restart docker.
systemctl daemon-reload
systemctl restart docker


apt-get update && apt-get install apt-transport-https curl  -y
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF

apt-get update
apt-get install kubelet kubeadm kubectl -y
apt-mark hold kubelet kubeadm kubectl

kubeadm init --control-plane-endpoint="$ENDPOINT" --pod-network-cidr=10.244.0.0/16
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/2140ac876ef134e0ed5af15c65e414cf26827915/Documentation/kube-flannel.yml
sysctl net.bridge.bridge-nf-call-iptables=1
kubectl taint nodes --all node-role.kubernetes.io/master-

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

export KUBECONFIG="$HOME/.kube/config"
echo "export KUBECONFIG=${KUBECONFIG}" >> /etc/environment
echo && cat $KUBECONFIG && echo
