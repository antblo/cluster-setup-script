#!/bin/bash

echo "apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
" | kubectl apply -f -

printf "\nAPI URL:\n"
cat $KUBECONFIG | grep server | awk '{print $2}'

printf "\nCA Certificate:\n"
cat $KUBECONFIG | grep certificate-authority-data | awk '{print $2}' | base64 -d

printf "\nService Token:\n"
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}') | grep token: | awk '{print $2}'
echo
