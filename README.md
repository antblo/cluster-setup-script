## Important links and resources

Run script:
```
bash <(curl https://gitlab.com/antonblomstrom97/cluster-setup-script/raw/master/cluster-setup.sh)
```

Keep in mind, the script may be outdated.

To understand what the script does so that you can reproduce it, go through: [Installing kubeadm, kubelet and kubectl](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)

If you want to connect to gitlab, here's a helper script for that too:\
https://gitlab.com/help/user/project/clusters/index.md#add-existing-kubernetes-cluster
```
bash <(curl https://gitlab.com/antonblomstrom97/cluster-setup-script/raw/master/connect-gitlab-cluster.sh)
```

And if you want to install the Gitlab managed applications, 
you need a tiller service account (not entirely sure if Gitlab does this for you though):
```
kubectl -n kube-system create serviceaccount tiller
kubectl create clusterrolebinding tiller --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
```

Helm is a little broken at the time of writing this however: https://github.com/helm/helm/issues/6374 \
But hopefully it'll be fixed in the near future and incorporated into gitlab.